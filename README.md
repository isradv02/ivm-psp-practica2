# Práctica 2 PSP - Israel David Valor Miró

Link proyecto en Gitlab: https://gitlab.com/isradv02/ivm-psp-practica2.git

!!! IMPORTANTE: No mover ninguno de los .jar de sus respectivas carpetas.

### Ejercicio1
El ejercicio1 nos permite ejecutar a través de argumentos comandos del sistema Linux[Al terminar se guarda el resultado en el fichero output.txt].

Para ejecutar el Ejercicio1 podemos:

 1. Abrir el proyecto en IntelliJ, desde allí Ejecutar el ejercicio1.
 2. Ir a la carpeta del proyecto (/out/artifacts/Ejercicio1_jar) y ejecutar el comando:(java -jar Ejercicio1.jar [comando a ejecutar])

 Ejemplo: java -jar Ejercicio1.jar ls -la

 !!! Si se ejecuta desde el IDE el fichero output.txt aparecerá en la carpeta del proyecto, en caso contrario en la carpeta donde se ejecuta el jar.


 ### Ejercicio2
 El ejercicio2 capta la entrada del usuario y devuelve un número aleatorio, si la entrada del usuario es "stop" el programa terminará[Al terminar se guarda el resultado en el fichero randoms.txt].

 Para ejecutar el Ejercicio2 podemos:

  1. Abrir el proyecto en IntelliJ, desde allí Ejecutar el ejercicio2.
  2. Ir a la carpeta del proyecto (/out/artifacts/Ejercicio2_jar) y ejecutar el comando:(java -jar Ejercicio2.jar)


  ### Ejercicio3
 El ejercicio3 capta la entrada del usuario y devuelve esa entrada en minúsculas, si la entrada del usuario es "finalizar" el programa terminará.

  Para ejecutar el Ejercicio1 podemos:

   1. Abrir el proyecto en IntelliJ, desde allí Ejecutar el ejercicio3.
   2. Ir a la carpeta del proyecto (/out/artifacts/Ejercicio3_jar) y ejecutar el comando:(java -jar Ejercicio3.jar)
