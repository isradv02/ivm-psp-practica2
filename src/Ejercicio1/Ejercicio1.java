package Ejercicio1;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Ejercicio1 {
    public static void main(String[] args) {
        //Si no hay argumentos da esta salida y termina el programa
        if (args.length < 1){
            System.out.println("No hay argumentos, el comando no se puede ejecutar.");
            System.exit(-1);
        }
        int exitValue;
        try{
            //Introduzco los argumentos sobre un ArrayList
            List<String> argumentos = new ArrayList<>();
            for (String arg:args) {
                argumentos.add(arg);
            }
            //Con el Arraylist antes creado se crea el nuevo proceso
            Process pr = new ProcessBuilder(argumentos).start();
            //Espera para comprobar si el subproceso termina antes de 2 segundos
            if(!pr.waitFor(2, TimeUnit.SECONDS)){
                //Salida si el tiempo termina antes que el subproceso
                System.out.println("El tiempo se ha agotado");
            } else{
                //Salida si el proceso termina antes del tiempo puesto(imprime el valor de ejecución)
                System.out.println("Valor de ejecución del proceso: "+(exitValue = pr.waitFor()));
                //Si el valor de salida es cero(no tiene errores) entonces se imprime la salida del proceso
                if(exitValue==0){
                    //Imprime la salida del subproceso
                    InputStream inStream = pr.getInputStream();
                    BufferedReader bf = new BufferedReader(new InputStreamReader(inStream));
                    BufferedWriter bw = Files.newBufferedWriter(new File("output.txt").toPath());
                    String linea;
                    while ((linea = bf.readLine())!=null){
                        bw.write(linea+"\n");
                    }
                    bf.close();
                    bw.close();
                } else{
                    //Imprime la lista de errores si hay alguno
                    System.out.println("Error en la ejecución del proceso hijo");
                    InputStream inStream = pr.getErrorStream();
                    BufferedReader bf = new BufferedReader(new InputStreamReader(inStream));
                    String linea;
                    while ((linea = bf.readLine())!=null){
                        System.out.println(linea);
                    }
                    bf.close();
                }
            }
        } catch (IOException e) {
            //Salida si da error IO
            System.out.println("Error I/O");
            e.printStackTrace();
        } catch (InterruptedException e) {
            //Salida si da error de interrupción
            System.out.println("Error de interrupción");
            e.printStackTrace();
        }
    }
}
