package Ejercicio2;

import java.io.*;
import java.nio.file.Files;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Ejercicio2 {
    public static void main(String[] args) {
        try {
            //Guardo y creo el processBuilder para hacer el subproceso que ejecuta minusculas.
            String[] command = new String[]{"java","-jar","Random10.jar"};
            ProcessBuilder pb = new ProcessBuilder(command);
            //Marco el directorio donde está el jar como predeterminado.
            pb.directory(new File(System.getProperty("user.dir")+"/src/Ejercicio2"));
            //Inicio el subproceso.
            Process pr = pb.start();
            //BufferedWriter para la entrada del subproceso(para poder enviar mensajes al subproceso).
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(pr.getOutputStream()));
            //BufferedReader para capturar la entrada del programa(del usuario).
            BufferedReader sysbr = new BufferedReader(new InputStreamReader(System.in));
            //Scanner para capturar mensajes salidos del subproceso.
            Scanner sc = new Scanner(pr.getInputStream());
            //BufferedWriter para escribir la salida sobre el fichero randoms.txt
            BufferedWriter bwtxt = Files.newBufferedWriter(new File("randoms.txt").toPath());

            //Este bucle lee la entrada del usuario y la manda al subproceso,
            //Después imprime la salida del subproceso y la escribe sobre randoms.txt
            String linea = sysbr.readLine();
            while(!linea.equalsIgnoreCase("stop")) {
                bw.write(linea);
                bw.newLine();
                bw.flush();
                String salida = sc.nextLine();
                System.out.println(salida);
                bwtxt.write(salida+"\n");
                linea = sysbr.readLine();
            }
            //Se cierran los buffered
            bwtxt.close();
            bw.close();
            sysbr.close();
            System.out.println("");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
