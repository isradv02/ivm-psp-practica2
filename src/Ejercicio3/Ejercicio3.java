package Ejercicio3;

import java.io.*;
import java.nio.file.Files;
import java.util.Scanner;

public class Ejercicio3 {
    public static void main(String[] args) {
        try {
            //Guardo y creo el processBuilder para hacer el subproceso que ejecuta minusculas.
            String[] command = new String[]{"java","-jar","Minusculas.jar"};
            ProcessBuilder pb = new ProcessBuilder(command);
            //Marco el directorio donde está el jar como predeterminado.
            pb.directory(new File(System.getProperty("user.dir")+"/src/Ejercicio3"));
            //Inicio el subproceso.
            Process pr = pb.start();
            //BufferedWriter para la entrada del subproceso(para poder enviar mensajes al subproceso).
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(pr.getOutputStream()));
            //BufferedReader para capturar la entrada del programa(del usuario).
            BufferedReader sysbr = new BufferedReader(new InputStreamReader(System.in));
            //Scanner para capturar mensajes salidos del subproceso.
            Scanner sc = new Scanner(pr.getInputStream());

            //Este bucle lee la entrada del usuario y la manda al subproceso,
            //Después imprime la salida del subproceso
            String linea = sysbr.readLine();
            while(!linea.equalsIgnoreCase("finalizar")) {
                bw.write(linea);
                bw.newLine();
                bw.flush();
                System.out.println(sc.nextLine());
                linea = sysbr.readLine();
            }
            //Se cierran los buffered
            bw.close();
            sysbr.close();
            System.out.println("");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
