package Ejercicio3;

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class Minusculas {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        String linea;
        while(!(linea = input.nextLine()).equalsIgnoreCase("finalizar")){
            System.out.println(linea.toLowerCase());
        }
    }
}
